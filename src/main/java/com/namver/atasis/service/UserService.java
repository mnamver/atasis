package com.namver.atasis.service;

import java.util.List;

import com.namver.atasis.entity.User;

public interface UserService {
	void add(User user);
	boolean login(User user);
}
