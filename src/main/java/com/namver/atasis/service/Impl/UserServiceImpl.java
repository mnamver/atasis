package com.namver.atasis.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.namver.atasis.dao.UserDao;
import com.namver.atasis.entity.User;
import com.namver.atasis.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;

	@Transactional
	@Override
	public void add(User user) {
		userDao.save(user);
	}

	@Override
	public boolean login(User user) {

		List<User> allUsers = userDao.list();
		for (User dbuser : allUsers) {
			if (dbuser.getFirstName() != null && (user.getFirstName().equals(dbuser.getFirstName()) && user.getPassword().equals(dbuser.getPassword()))) {
				return true;
			}
		}
		return false;

	}

}
