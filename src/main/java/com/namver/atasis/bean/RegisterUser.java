package com.namver.atasis.bean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import org.springframework.util.StringUtils;

import com.namver.atasis.entity.User;
import com.namver.atasis.service.UserService;


@ManagedBean
@SessionScoped
public class RegisterUser {
	
	private String errorMsg;

	@ManagedProperty("#{userServiceImpl}")
	private UserService userService;

	private User user = new User();
	
	public void onload() {
		System.out.println("onload start");
	
	}
	
	public String login() {
		System.out.println("login start");
		if(StringUtils.isEmpty(user.getFirstName()) || StringUtils.isEmpty(user.getPassword())) {
			  errorMsg = "L�tfen giri� bilgilerini eksiksiz doldurunuz";
			  return "";
		}	   
					
		boolean success = userService.login(user);

		if (success) {
			return "/empty.jsf?faces-redirect=true";
		} else {
			errorMsg = "Kullan�c� ad�n�z ya da �ifreniz yanl��.";
			return "";
		}
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

}
