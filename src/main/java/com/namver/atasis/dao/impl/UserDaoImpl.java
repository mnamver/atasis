package com.namver.atasis.dao.impl;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.namver.atasis.dao.UserDao;
import com.namver.atasis.entity.User;

@Repository
public class UserDaoImpl implements UserDao {

	@Autowired
	private SessionFactory sessionFactory;
	

	@Override
	public void save(User user) {
		sessionFactory.getCurrentSession().save(user);
	}


	@Override
	public List<User> list() {
		CriteriaQuery<User> criteria =	sessionFactory.getCurrentSession().getCriteriaBuilder().createQuery(User.class);
		criteria.from(User.class);
		return sessionFactory.getCurrentSession().createQuery(criteria).getResultList();
	}

}   
