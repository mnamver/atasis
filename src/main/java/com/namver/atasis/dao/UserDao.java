package com.namver.atasis.dao;



import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.namver.atasis.entity.User;

@Repository
@Transactional
public interface UserDao{
	public List<User> list();
	
	void save(User user);
}
